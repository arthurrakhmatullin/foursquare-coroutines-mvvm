package com.mircod.fella.coroutines.data.models

data class NetworkResponse(val meta: Meta, val response: Response)