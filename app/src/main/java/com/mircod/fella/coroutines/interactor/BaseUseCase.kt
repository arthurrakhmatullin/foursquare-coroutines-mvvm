package com.mircod.fella.coroutines.interactor

import kotlinx.coroutines.Job

abstract class BaseUseCase {

    private var job: Job? = null

    fun cancelJob(){
        if (job!=null && job!!.isActive){
            job?.cancel()
        }
    }

}