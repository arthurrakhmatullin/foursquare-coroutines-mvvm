package com.mircod.fella.coroutines.data.models

data class Meta(val code: Int, val requestId: String)