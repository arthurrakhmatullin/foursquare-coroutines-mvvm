package com.mircod.fella.coroutines.interactor

import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.core.Response
import com.mircod.fella.coroutines.data.datasource.remote.RemoteFoursquareDataSource
import com.mircod.fella.coroutines.data.repository.FoursquareRepository
import javax.inject.Inject

class InitialLoad @Inject constructor(val repository: FoursquareRepository): BaseUseCase() {
    suspend operator fun invoke(ll: String): Response<List<FoursquareModel>>{
      return repository.getData(ll)
    }
}