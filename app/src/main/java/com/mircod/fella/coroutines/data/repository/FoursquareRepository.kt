package com.mircod.fella.coroutines.data.repository

import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.core.Response
import com.mircod.fella.coroutines.data.datasource.local.LocalFoursquareDataSource
import com.mircod.fella.coroutines.data.datasource.remote.RemoteFoursquareDataSource
import timber.log.Timber
import javax.inject.Inject

class FoursquareRepository @Inject constructor(
    private val remote: RemoteFoursquareDataSource,
    private val local: LocalFoursquareDataSource
) : IRepository<FoursquareModel> {

    override suspend fun getData(query: String): Response<List<FoursquareModel>> {
        val localData = local.getPlacesByLL(query)
        return when (localData) {
            is Response.Success -> {
                if (localData.data.isNotEmpty()) {
                    remote.getPlacesByLL(query)
                    localData
                } else {
                    remote.getPlacesByLL(query)
                }
            }
            is Response.Error -> {
                Timber.e(localData.exception)
                remote.getPlacesByLL(query)
            }
        }
    }

    override suspend fun updateData(t: FoursquareModel) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun deleteData(t: FoursquareModel) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}