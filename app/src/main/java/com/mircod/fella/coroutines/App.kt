package com.mircod.fella.coroutines

import android.app.Application
import com.mircod.fella.coroutines.di.AppComponent
import com.mircod.fella.coroutines.di.ApplicationModule
import com.mircod.fella.coroutines.di.DaggerAppComponent
import com.mircod.fella.coroutines.di.NetworkModule
import com.mircod.fella.coroutines.util.TimberTree
import timber.log.Timber

class App: Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(TimberTree())
        appComponent = DaggerAppComponent.builder()
            .networkModule(NetworkModule(this))
            .applicationModule((ApplicationModule(this)))
            .build()
    }
}