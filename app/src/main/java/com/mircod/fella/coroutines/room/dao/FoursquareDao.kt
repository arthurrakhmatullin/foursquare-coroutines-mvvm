package com.mircod.fella.coroutines.room.dao

import androidx.room.*
import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.data.datasource.local.IFoursquareDao

@Dao
interface FoursquareDao : IFoursquareDao {
    @Query("SELECT * FROM foursquaremodel")
    override fun select(): List<FoursquareModel>


    @Query("""
        SELECT * FROM foursquaremodel
        WHERE lat LIKE :query1 AND lng LIKE :query2
    """)
    override fun selectByQuery(query1: String, query2: String): List<FoursquareModel>

    @Delete
    override fun delete(element: FoursquareModel)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    override fun update(element: FoursquareModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun insert(element: FoursquareModel)
}