package com.mircod.fella.coroutines.util

import androidx.room.TypeConverter
import com.mircod.fella.coroutines.data.models.Category
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types


class CategoryConverter {
    val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    val listMyData = Types.newParameterizedType(List::class.java, Category::class.java)
    val jsonAdapter : JsonAdapter<List<Category>> = moshi.adapter(listMyData)

    @TypeConverter
    fun fromStringToCategory(json: String): List<Category>? = jsonAdapter.fromJson(json)


    @TypeConverter
    fun fromCategoryListToString(categoryList: List<Category>?): String? = jsonAdapter.toJson(categoryList)
}

class StringConverter{
    val moshi = Moshi.Builder().build()
    val listMyData = Types.newParameterizedType(List::class.java, String::class.java)
    val jsonAdapter : JsonAdapter<List<String>> = moshi.adapter(listMyData)

    @TypeConverter
    fun fromStringToListString(json: String): List<String>? = jsonAdapter.fromJson(json)

    @TypeConverter
    fun fromStringListToString(stringList: List<String>?): String? = jsonAdapter.toJson(stringList)
}