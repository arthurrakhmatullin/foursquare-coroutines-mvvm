package com.mircod.fella.coroutines.ui.venue

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.core.Response
import com.mircod.fella.coroutines.interactor.InitialLoad
import kotlinx.coroutines.*
import javax.inject.Inject

class VenuesViewModel constructor(private val useCase: InitialLoad) : ViewModel() {
    private val _liveData: MutableLiveData<List<FoursquareModel>> = MutableLiveData()
    val liveData: LiveData<List<FoursquareModel>>
        get() = _liveData
    private val _errorLiveData: MutableLiveData<Throwable> = MutableLiveData()
    val errorLiveData: LiveData<Throwable>
        get() = _errorLiveData

    private var job: Job? = null

    fun requestVenues(location: Location){
        job = GlobalScope.launch(Dispatchers.IO, CoroutineStart.DEFAULT, null, {
            val result = useCase.invoke("${location.latitude},${location.longitude}")
            when (result) {
                is Response.Success -> {
                    _liveData.postValue(result.data)
                }
                is Response.Error -> {
                    _errorLiveData.postValue(result.exception)
                }
            }
        })
    }

    override fun onCleared() {
        if (job!= null && job!!.isActive)
            job?.cancel()
        super.onCleared()
    }
}
