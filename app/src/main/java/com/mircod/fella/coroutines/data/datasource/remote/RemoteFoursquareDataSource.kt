package com.mircod.fella.coroutines.data.datasource.remote

import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.core.Response
import com.mircod.fella.coroutines.data.datasource.FoursquareDataSource
import com.mircod.fella.coroutines.data.datasource.local.IFoursquareDao
import com.mircod.fella.coroutines.util.callApi
import com.mircod.fella.coroutines.util.mapToModel
import java.io.IOException
import javax.inject.Inject

class RemoteFoursquareDataSource @Inject constructor(private val api: FoursquareApi, private val dao: IFoursquareDao): FoursquareDataSource {

    private suspend fun getNearbyPlaces(ll: String): Response<List<FoursquareModel>> {
        val response = api.getNearlyVenues(ll).await()
        return if (response.meta.code == 200) {
            val list = arrayListOf<FoursquareModel>()
            response.response.venues.forEach {
                val f = mapToModel(it)
                dao.insert(f)
                list.add(f)
            }
            Response.Success(list)
        } else Response.Error(IOException("Error on loading Venues via network"))
    }

    override suspend fun getPlacesByLL(ll: String): Response<List<FoursquareModel>> = callApi(
        call = { getNearbyPlaces(ll) },
        error = "Unable to load  places"
    )
}