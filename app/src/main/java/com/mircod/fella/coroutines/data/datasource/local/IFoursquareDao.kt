package com.mircod.fella.coroutines.data.datasource.local

import com.mircod.fella.coroutines.core.FoursquareModel

interface IFoursquareDao : IDao<FoursquareModel>