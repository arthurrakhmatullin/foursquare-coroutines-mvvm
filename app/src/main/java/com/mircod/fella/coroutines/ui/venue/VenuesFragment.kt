package com.mircod.fella.coroutines.ui.venue

import android.annotation.SuppressLint
import android.location.Location
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import com.mircod.fella.coroutines.App
import com.mircod.fella.coroutines.R
import com.mircod.fella.coroutines.util.ViewModelFactory
import kotlinx.android.synthetic.main.start_fragment.*
import timber.log.Timber
import javax.inject.Inject

private const val REQUESTING_LOCATION_UPDATES_KEY: String = "123123"
private const val RECYCLER_VIEW_STATE: String = "key_recycler"

class VenuesFragment : Fragment() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private var requestingLocationUpdates = true
    private var recyclerViewState: Parcelable? = null
    private lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var factory: ViewModelFactory

    private val locationRequest = LocationRequest().apply {
        interval = 10000
        fastestInterval = 5000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private lateinit var viewModel: VenuesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.start_fragment, container, false)
    }

    @SuppressLint("MissingPermission")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        layoutManager = LinearLayoutManager(context)
        updateValuesFromBundle(savedInstanceState)
        App.appComponent.inject(this)
        val mAdapter = VenuesAdapter()
        viewModel = ViewModelProviders.of(this, factory).get(VenuesViewModel::class.java)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null)
                    viewModel.requestVenues(location)
            }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                requestingLocationUpdates = false
                for (location in locationResult.locations) {
//                    viewModel.requestVenues(location)
                }
            }
        }

        rv_venues.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(context)
        }
        viewModel.liveData.observe(this, Observer {
            mAdapter.addVenues(it)
            Timber.e(it.toString())
        })

        viewModel.errorLiveData.observe(this, Observer {
            Timber.e(it)
        })

    }

    private fun updateValuesFromBundle(savedInstanceState: Bundle?) {
        savedInstanceState ?: return
        if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
            requestingLocationUpdates = savedInstanceState.getBoolean(
                REQUESTING_LOCATION_UPDATES_KEY
            )
        }
        if (savedInstanceState.keySet().contains(RECYCLER_VIEW_STATE)) {
            recyclerViewState = savedInstanceState.getParcelable(
                RECYCLER_VIEW_STATE
            )
            layoutManager.onRestoreInstanceState(recyclerViewState)
        }
    }

    override fun onResume() {
        super.onResume()
        if (requestingLocationUpdates) startLocationUpdates()
    }

    override fun onPause() {
        super.onPause()
        recyclerViewState = layoutManager.onSaveInstanceState()
        stopLocationUpdates()
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
        requestingLocationUpdates = true
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
    }


    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, requestingLocationUpdates)
        outState.putParcelable(RECYCLER_VIEW_STATE, recyclerViewState)
        super.onSaveInstanceState(outState)
    }

    companion object {
        fun newInstance() = VenuesFragment()
    }
}
