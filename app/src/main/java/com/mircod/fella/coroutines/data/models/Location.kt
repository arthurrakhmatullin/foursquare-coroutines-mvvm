package com.mircod.fella.coroutines.data.models

import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class Location(
    val lat: Double,
    val lng: Double,
    val distance: Int,
    val formatedAddress: List<String>?
)