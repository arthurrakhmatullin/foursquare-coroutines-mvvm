package com.mircod.fella.coroutines.data.datasource.remote

import com.mircod.fella.coroutines.data.models.NetworkResponse
import com.mircod.fella.coroutines.util.CLIENT_ID
import com.mircod.fella.coroutines.util.CLIENT_SECRET
import com.mircod.fella.coroutines.util.VERSION
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface FoursquareApi {

    @GET("venues/search")
    fun getNearlyVenues(
        @Query("ll") ll: String,
        @Query("client_id") clientID: String = CLIENT_ID,
        @Query("client_secret") clientSecret: String = CLIENT_SECRET,
        @Query("v") version: String = VERSION
    ): Deferred<NetworkResponse>

}