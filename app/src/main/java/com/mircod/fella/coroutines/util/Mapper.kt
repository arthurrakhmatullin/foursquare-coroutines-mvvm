package com.mircod.fella.coroutines.util

import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.data.models.Venue

fun mapToModel(venue: Venue, fModel: FoursquareModel? = null): FoursquareModel {
    return if (fModel == null) {
        val iconString: String? = if (!venue.categories.isNullOrEmpty()) {
            venue.categories.first().icon.prefix + "bg_64" + venue.categories.first().icon.suffix
        } else null

        FoursquareModel(
            icon = iconString,
            id = venue.id,
            name = venue.name,
            category = venue.categories,
            location = venue.location
        )
    } else {
        //TODO add photos, tips and etc to previous model
        fModel
    }
}