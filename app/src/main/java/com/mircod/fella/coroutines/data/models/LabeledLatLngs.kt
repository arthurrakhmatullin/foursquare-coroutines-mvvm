package com.mircod.fella.coroutines.data.models

data class LabeledLatLngs(val label: String, val lat: Double, val lng: Double)