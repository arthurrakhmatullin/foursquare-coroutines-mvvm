package com.mircod.fella.coroutines.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.room.dao.FoursquareDao
import com.mircod.fella.coroutines.util.CategoryConverter
import com.mircod.fella.coroutines.util.StringConverter

@Database(entities = [FoursquareModel::class],version = 1, exportSchema = false)
@TypeConverters(CategoryConverter::class, StringConverter::class)
abstract class AppDatabase : RoomDatabase(){
    abstract fun venuesDao(): FoursquareDao
}