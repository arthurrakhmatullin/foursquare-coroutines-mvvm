package com.mircod.fella.coroutines.data.datasource.local

import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.core.Response
import com.mircod.fella.coroutines.data.datasource.FoursquareDataSource
import com.mircod.fella.coroutines.util.callApi
import java.io.IOException
import javax.inject.Inject

class LocalFoursquareDataSource @Inject constructor(private val dao: IFoursquareDao) : FoursquareDataSource {

    override suspend fun getPlacesByLL(ll: String): Response<List<FoursquareModel>> = callApi(
        call = { getNearbyPlaces(ll) },
        error = "Unable to load  places"
    )

    private fun getNearbyPlaces(ll: String): Response<List<FoursquareModel>> {
        val arr = ll.split(",")
        val data = dao.selectByQuery(arr[0].split(".").first()+"%", arr[1].split(".").first()+"%")
//        val data = dao.select()
        return if (data.isNotEmpty())
            Response.Success(data)
        else
            Response.Error(IOException("Error on loading Venues from db"))
    }
}