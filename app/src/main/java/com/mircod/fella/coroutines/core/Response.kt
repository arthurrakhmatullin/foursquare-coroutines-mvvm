package com.mircod.fella.coroutines.core

import java.lang.Exception

sealed class Response<out T: Any> {
    data class Success<out T: Any>(val data: T): Response<T>()
    data class Error(val exception: Exception): Response<Nothing>()
}