package com.mircod.fella.coroutines.data.models

data class Response(val venues: List<Venue>)