package com.mircod.fella.coroutines.data.datasource

import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.core.Response

interface FoursquareDataSource {
    suspend fun getPlacesByLL(ll: String): Response<List<FoursquareModel>>
}