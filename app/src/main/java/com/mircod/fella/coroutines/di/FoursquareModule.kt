package com.mircod.fella.coroutines.di

import androidx.lifecycle.ViewModelProvider
import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.data.datasource.local.IFoursquareDao
import com.mircod.fella.coroutines.data.datasource.local.LocalFoursquareDataSource
import com.mircod.fella.coroutines.data.datasource.remote.FoursquareApi
import com.mircod.fella.coroutines.data.datasource.remote.RemoteFoursquareDataSource
import com.mircod.fella.coroutines.data.repository.FoursquareRepository
import com.mircod.fella.coroutines.data.repository.IRepository
import com.mircod.fella.coroutines.interactor.InitialLoad
import com.mircod.fella.coroutines.util.ViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FoursquareModule {
    @Singleton
    @Provides
    fun provideRemoteDataSource(api: FoursquareApi, dao: IFoursquareDao): RemoteFoursquareDataSource =
        RemoteFoursquareDataSource(api, dao)

    @Singleton
    @Provides
    fun provideInitialUseCase(dataSource: FoursquareRepository): InitialLoad = InitialLoad(dataSource)

    @Singleton
    @Provides
    fun provideRepository(local: LocalFoursquareDataSource, remote: RemoteFoursquareDataSource) : IRepository<FoursquareModel> =
        FoursquareRepository(local = local, remote = remote)

    @Singleton
    @Provides
    fun provideFactory(useCase: InitialLoad) : ViewModelProvider.Factory =
            ViewModelFactory(useCase)
}