package com.mircod.fella.coroutines.core

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.mircod.fella.coroutines.data.models.Category
import com.mircod.fella.coroutines.data.models.Location

@Entity
data class FoursquareModel(
    @PrimaryKey
    val id: String,
    val name: String,
    val icon : String?,
    @Embedded
    val location: Location,
    val category: List<Category>,
    val photos: List<String>? = null,
    val bestPhoto: String? = null,
    val tips: String? = null
)