package com.mircod.fella.coroutines.data.datasource.local

interface IDao<T> {
    fun select(): List<T>
    fun selectByQuery(query1: String, query2: String): List<T>
    fun delete(element: T)
    fun update(element: T)
    fun insert(element: T)
}