package com.mircod.fella.coroutines.ui.venue

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mircod.fella.coroutines.R
import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.databinding.VenueItemBinding

class VenuesAdapter : RecyclerView.Adapter<VenuesAdapter.VenuesViewHolder>() {
    private var venuesList: ArrayList<FoursquareModel> = arrayListOf()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): VenuesViewHolder {
        val layoutInflater = LayoutInflater.from(p0.context)
        val binding: VenueItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.venue_item, p0, false)
        return VenuesViewHolder(binding)
    }

    fun addVenues(venues: List<FoursquareModel>) {
        venues.forEach {
            if (venuesList.contains(it)) return
            venuesList.add(it)
            notifyItemInserted(this.itemCount)
        }
    }

    override fun getItemCount() = venuesList.size

    override fun onBindViewHolder(holder: VenuesViewHolder, position: Int) {
        holder.bind(venuesList[position])
    }

    fun updateDevice(warmDevice: ArrayList<FoursquareModel>) {
        venuesList = warmDevice
        notifyDataSetChanged()
    }

    inner class VenuesViewHolder(val binding: VenueItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(venue: FoursquareModel) {
            binding.venue = venue
            binding.executePendingBindings()
        }
    }
}