package com.mircod.fella.coroutines.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.mircod.fella.coroutines.core.FoursquareModel
import com.mircod.fella.coroutines.data.datasource.local.IDao
import com.mircod.fella.coroutines.data.datasource.local.IFoursquareDao
import com.mircod.fella.coroutines.data.datasource.local.LocalFoursquareDataSource
import com.mircod.fella.coroutines.room.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(val app: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideFoursquareDatabase(app: Context): AppDatabase =
        Room.databaseBuilder(
            app,
            AppDatabase::class.java,
            "foursquare_db"
        ).fallbackToDestructiveMigration().build()

    @Provides
    @Singleton
    fun provideFoursquareDao(database: AppDatabase): IFoursquareDao = database.venuesDao()

    @Singleton
    @Provides
    fun provideRemoteDataSource(dao: IFoursquareDao): LocalFoursquareDataSource = LocalFoursquareDataSource(dao)
}