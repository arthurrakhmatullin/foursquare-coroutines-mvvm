package com.mircod.fella.coroutines.util

import com.mircod.fella.coroutines.core.Response
import java.io.IOException
import java.lang.Exception

suspend fun <T : Any> callApi(
    call: suspend () -> Response<T>,
    error: String
): Response<T> {
    return try {
        call()
    } catch (e: Exception) {
        Response.Error(IOException(error, e))
    }
}