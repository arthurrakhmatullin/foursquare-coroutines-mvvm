package com.mircod.fella.coroutines.di

import com.mircod.fella.coroutines.ui.venue.VenuesFragment
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        ApplicationModule::class,
        NetworkModule::class,
        FoursquareModule::class
    ]
)
@Singleton
interface AppComponent {
    fun inject(fr: VenuesFragment)
}