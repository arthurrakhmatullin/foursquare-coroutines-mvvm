package com.mircod.fella.coroutines.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mircod.fella.coroutines.interactor.InitialLoad
import com.mircod.fella.coroutines.ui.venue.VenuesViewModel
import javax.inject.Inject

class ViewModelFactory  @Inject constructor(var useCase: InitialLoad): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return  if (modelClass.isAssignableFrom(VenuesViewModel::class.java))
            VenuesViewModel(useCase) as T
        else throw IllegalArgumentException("ViewModel Not Found")
    }
}