package com.mircod.fella.coroutines.data.models

data class Icon(val prefix: String, val suffix: String)