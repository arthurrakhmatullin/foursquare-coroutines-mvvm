package com.mircod.fella.coroutines.data.repository

import com.mircod.fella.coroutines.core.Response

interface IRepository<T> {
    suspend fun getData(query: String):Response<List<T>>
    suspend fun updateData(t: T)
    suspend fun deleteData(t: T)
}