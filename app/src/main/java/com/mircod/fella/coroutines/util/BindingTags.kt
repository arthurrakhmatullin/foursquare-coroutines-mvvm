package com.mircod.fella.coroutines.util

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("load_image")
fun loadImage(view: ImageView, uri: String?) {
    if (uri == null) return
    Picasso.get().load(uri).into(view)
}

@BindingAdapter("setFormattedAddress")
fun setFormattedAddress(view: TextView, formattedAddr: List<String>?) {
    formattedAddr?.forEach {
        if (view.text.isNotEmpty())
            view.text = "${view.text} \r\n $it"
        else
            view.text = it
    }
}